=== WP Facebook FanBox ===
Contributors: vivacityinfotech.jaipur
Donate link: http://tinyurl.com/owxtkmt
Tags:  wp fanbox,fanbox,facebook fanbox,wp facebook fanbox,wp facebook fanbox widget,facebook likebox, facebook page timeline,facebook likebox widget,facebook likebox widget for wordpress,wp facebook likebox widget,likebox, facebook
Requires at least: 3.0
Tested up to: 3.8
Stable Tag: 1.0
License: GPLv2 or later

WP FaceBook FanBox - A social plugin that allows page owners to promote their Pages and embed a page feed on their websites.


== Description ==
WP FaceBook FanBox is a social plugin that allows page owners to promote their Pages and embed a simple feed of content from a Page into other sites.

It is a special version of the Like Button designed only for Facebook Pages. It allows admins to promote their Pages and embed a simple feed of content from a Page into other sites.

= Features =

    *Easy install
    *Very easy to configure.
    *Displays recent activity from your Facebook Page and encourages new Like actions.
    *Multiple instance so you can use the  widget in multiple places.
    *Configurable Facebook color scheme(Light/Dark).
    *Lightweight and loading fast

= Rate Us / Feedback =

Please take the time to let us and others know about your experiences by leaving a review, so that we can improve the plugin for you and other users.

= Want More? =

If You Want more functionality or some modifications, just drop us a line what you want and We will try to add or modify the plugin functions.

== Installation ==

Installation consists of following steps:

1. Upload "WP FaceBook Fan Box" to the /wp-content/plugins/ directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Place the "WP FaceBook Fan Box" Widget on your sidebar and add facebook appID with other details of FaceBook.


== Frequently Asked Questions ==
= How to Create a Facebook App =

1. Visit the Facebook Developers Site: The first thing you need to do is head on down to https://developers.facebook.com. In order to create an account, all you need to do is click on the “Sign In” link.

2. Sign in with your Facebook Account: Next, sign in with the Facebook account you want to associate with your app. You do have a Facebook account don’t you?!

3. To create a Facebook app, click on Apps on the top of the page and select 'Create a New App'.
	This will create a popup box will prompt you to enter two things:
	* An display name.This is the app name used when stories are published to Facebook and in a number of other places. 
	* Use Open Graph Getting Started or something similar.
	* A namespace. Not required for this guide so just leave it blank
	* A category.This will determine the kind of app that you're building. For now, just select Communication.

4. Once you've created your app you'll be provided with a dashboard that shows User and API Stats. From this screen, click on Settings on the left.

5. If you have any troubles creating the app or for more in depth information please visit https://developers.facebook.com/docs/opengraph/getting-started/


= Can I use the regular Like Button with my Facebook Page =
Yes. This can be useful if you need a more compact layout than the Like Box can provide.

= How can I allow people to like my webpages? =
This Like Box is designed only for use with Facebook Pages. For any other purposes you should use the Like Button

= How do I know when someone clicked on a Like button on my site? =
The XFBML and HTML5 versions of the button allow you to subscribe to the 'edge.create' event in the Facebook SDK for JavaScript through FB.Event.subscribe. This JavaScript event will fire any time a button is clicked.


== Available Languages ==

    English
    
== Screenshots ==
1. WP Facebook FanBox plugin installed and appears in the widgets area.
2. WP Facebook FanBox widget under Appearence->Widgets.
3. Plugin Settings page to configure various settings.
4. Widget displayed in the front end.

== Changelog ==
= 1.0 =
* Initial release
